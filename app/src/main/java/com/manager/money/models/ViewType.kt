package com.manager.money.models

enum class ViewType {
    HEADER, NORMAL
}