package com.manager.money.models

enum class CategoryType {
    NONE, ENTERTAINMENT, MISC, SHOPPING, SPORT, TRAVEL, HEADER
}