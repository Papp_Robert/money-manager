package com.manager.money.models

data class IconModel(val viewType: ViewType, val text: String, val categoryType: CategoryType) {

    val safeViewType: ViewType
        get() = viewType

    val safeText: String
        get() = text

    val safeCategoryType: CategoryType
        get() = categoryType
}