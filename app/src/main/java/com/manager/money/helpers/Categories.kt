package com.manager.money.helpers

import android.content.Context
import com.manager.money.constants.Constants
import com.manager.money.models.CategoryType
import com.manager.money.models.IconModel
import com.manager.money.models.ViewType
import com.manager.money.moneymanager.R
import com.manager.money.viewModels.ChooseCategoriesFromAllPossibleCategoriesViewModel

class Categories(val context: Context) {

    val entertainmentCategoryIcons
        get() = setHeaderForIcons(CategoryType.ENTERTAINMENT, context.resources.getStringArray(R.array.category_icons_entertainment))

    val miscCategoryIcons
        get() = setHeaderForIcons(CategoryType.MISC, context.resources.getStringArray(R.array.category_icons_misc))

    val shoppingCategoryIcons
        get() = setHeaderForIcons(CategoryType.SHOPPING, context.resources.getStringArray(R.array.category_icons_shopping))
    val sportCategoryIcons
        get() = setHeaderForIcons(CategoryType.SPORT, context.resources.getStringArray(R.array.category_icons_sports))
    val travelCategoryIcons
        get() = setHeaderForIcons(CategoryType.TRAVEL, context.resources.getStringArray(R.array.category_icons_travel))

    val allCategoryIcons
        get() = entertainmentCategoryIcons + miscCategoryIcons + shoppingCategoryIcons + sportCategoryIcons + travelCategoryIcons

    private fun setHeaderForIcons(categoryType: CategoryType, icons: Array<String>): ArrayList<IconModel> {
        val iconsWithHeaderValue = arrayListOf<IconModel>()
        iconsWithHeaderValue.add(IconModel(ViewType.HEADER, categoryType.toString(), CategoryType.HEADER))

        val iconModels = mutableListOf<IconModel>()

        for (icon in icons) {
            iconModels.add(IconModel(ViewType.NORMAL, icon, categoryType))
        }

        iconsWithHeaderValue.addAll(iconModels)
        fillRecyclerViewWithEmptyValues(iconsWithHeaderValue)

        return iconsWithHeaderValue
    }

    private fun fillRecyclerViewWithEmptyValues(iconsWithHeaderValue: ArrayList<IconModel>) {
        while (iconsWithHeaderValue.count() % ChooseCategoriesFromAllPossibleCategoriesViewModel.spanCount != 1) {
            iconsWithHeaderValue.add(IconModel(ViewType.NORMAL, "", CategoryType.NONE))
        }
    }
}