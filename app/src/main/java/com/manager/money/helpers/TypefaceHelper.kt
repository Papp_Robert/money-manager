package com.manager.money.helpers

import android.content.Context
import android.graphics.Typeface
import java.util.*

object TypefaceHelper {

    private val cache = Hashtable<String, Typeface>()

    operator fun get(c: Context, name: String): Typeface? {
        synchronized(cache) {
            if (!cache.containsKey(name)) {
                val t = Typeface.createFromAsset(
                        c.assets,
                        String.format("fonts/%s.ttf", name)
                )
                cache[name] = t
            }
            return cache[name]
        }
    }

}
