package com.manager.money.helpers

import android.content.Context
import com.manager.money.database.data.Category
import com.manager.money.models.IconModel
import com.manager.money.models.ViewType
import com.manager.money.repositories.interfaces.ICategoryRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SaveCategoriesToRepository(private val categoryRepository: ICategoryRepository, val context: Context) {

    private val categories = Categories(context)

    fun saveAllCategoriesToRepository() {
        saveEntertainmentCategoryIcons()
        saveMiscCategoryIcons()
        saveShoppingCategoryIcons()
        saveSportCategoryIcons()
        saveTravelCategoryIcons()
    }

    fun saveAllCategoriesToDbWithClear() {
        categoryRepository.deleteAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    saveAllCategoriesToRepository()
                }, {})
    }

    private fun saveEntertainmentCategoryIcons() {
        saveCategoryListToRepository(categories.entertainmentCategoryIcons, "Entertainment")
    }

    private fun saveMiscCategoryIcons() {
        saveCategoryListToRepository(categories.miscCategoryIcons, "Misc")
    }

    private fun saveShoppingCategoryIcons() {
        saveCategoryListToRepository(categories.shoppingCategoryIcons, "Shopping")
    }

    private fun saveSportCategoryIcons() {
        saveCategoryListToRepository(categories.sportCategoryIcons, "Sport")
    }

    private fun saveTravelCategoryIcons() {
        saveCategoryListToRepository(categories.travelCategoryIcons, "Travel")
    }

    private fun saveCategoryListToRepository(categoryIcons: ArrayList<IconModel>, categoryName: String) {
        for (categoryIcon in categoryIcons) {
            if (categoryIcon.safeViewType == ViewType.NORMAL) {
                val category = Category(categoryName, categoryIcon.safeText)
                categoryRepository.insert(category)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({}, {})
            }
        }
    }


}