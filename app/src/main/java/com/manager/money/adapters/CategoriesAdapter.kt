package com.manager.money.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.manager.money.database.data.Category
import com.manager.money.models.IconModel
import com.manager.money.models.ViewType
import com.manager.money.moneymanager.R
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.item_icon.view.*

class CategoriesAdapter(private val modelIcons: List<IconModel>, val context: Context) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    private var selectedPositions = mutableListOf<Int>()
    var selectedCategories = mutableListOf<Category>()

    companion object {
        const val HEAD_TYPE: Int = 0
        const val NORMAL_TYPE = 1
    }

    override fun getItemViewType(position: Int): Int {
        when (modelIcons[position].safeViewType == ViewType.HEADER) {
            true -> return HEAD_TYPE
            false -> return NORMAL_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View
        val holder: ViewHolder

        if (viewType == HEAD_TYPE) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
            holder = ViewHolder(view, viewType)
            return holder
        } else if (viewType == NORMAL_TYPE) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_icon, parent, false)
            holder = ViewHolder(view, viewType)
            return holder
        }

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_icon, parent, false))
    }

    override fun getItemCount(): Int {
        return modelIcons.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val positionHadBeenAlreadyAdded = selectedPositions.contains(position)
        when (positionHadBeenAlreadyAdded) {
            true -> holder.iconName?.setBackgroundColor(Color.parseColor("#000000"))
            false -> holder.iconName?.setBackgroundColor(Color.parseColor("#ffffff"))

        }
        holder.iconName?.text = modelIcons[position].text

        holder.iconName?.setOnClickListener {
            if (!positionHadBeenAlreadyAdded) {
                selectedPositions.add(position)
                selectedCategories.add(Category(modelIcons[position].safeCategoryType.toString(), modelIcons[position].safeText))
            } else {
                selectedPositions.remove(position)
                selectedCategories.add(Category(modelIcons[position].safeCategoryType.toString(), modelIcons[position].safeText))
            }
            notifyDataSetChanged()
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var iconName: TextView? = null

        constructor(view: View, viewType: Int) : this(view) {
            if (viewType == HEAD_TYPE) {
                iconName = view.headerText
            } else {
                iconName = view.iconName
            }

        }
    }
}