package com.manager.money.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.manager.money.database.data.Purchase
import com.manager.money.moneymanager.R
import com.manager.money.viewHolders.PurchaseListViewHolder

class PurchaseAdapter(private val items : List<Purchase>?, private val context: Context) : RecyclerView.Adapter<PurchaseListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PurchaseListViewHolder {
        return PurchaseListViewHolder(LayoutInflater.from(context).inflate(R.layout.expense_list_item, parent,false))
    }

    override fun onBindViewHolder(holder: PurchaseListViewHolder, position: Int) {
        val item = items?.get(position)
        holder.noteTextField.text = item?.note
        holder.valueTextField.text = item?.value.toString()
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

}
