package com.manager.money.database.dao.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * The account representation in the database.
 * Name of the account, currency means that database is in HUF, or EUR etc. Type is cash, or credit card or other
 * Example = "KP", "Huf", "Cash"
 */

@Entity(tableName = "Account")
data class Account(
        @PrimaryKey(autoGenerate = true)
        var id: Long?,
        @ColumnInfo(name = "name")
        var name: String,
        @ColumnInfo(name = "currency")
        var currency: String,
        @ColumnInfo(name = "type")
        var type: String,
        @ColumnInfo(name = "iconName")
        var iconName: String
)
