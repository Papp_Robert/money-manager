package com.manager.money.database.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.manager.money.database.dao.AccountDao
import com.manager.money.database.dao.CategoryDao
import com.manager.money.database.dao.PurchaseDao
import com.manager.money.database.dao.data.Account
import com.manager.money.database.data.Category
import com.manager.money.database.data.Purchase

@Database(
        entities = [
            Account::class,
            Category::class,
            Purchase::class
        ],
        version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun accountDao(): AccountDao
    abstract fun categoryDao(): CategoryDao
    abstract fun purchaseDao(): PurchaseDao
}
