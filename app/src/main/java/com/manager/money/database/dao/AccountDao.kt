package com.manager.money.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.manager.money.database.dao.data.Account

@Dao
interface AccountDao {

    @Query("SELECT * FROM account")
    fun getAll(): LiveData<List<Account>>

    @Query("SELECT * FROM account WHERE account.id =:accountDataId")
    fun getAccountById(accountDataId: Long): LiveData<Account>

    @Insert(onConflict = REPLACE)
    fun insert(accountData: Account)

    @Query("DELETE from account")
    fun deleteAll()
}