package com.manager.money.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.manager.money.database.data.Purchase

@Dao
interface PurchaseDao {

    @Query("SELECT * FROM purchase")
    fun getAll(): LiveData<List<Purchase>>

    @Query("SELECT * FROM purchase WHERE purchase.id =:purchaseId")
    fun getPurchaseById(purchaseId: Long): LiveData<Purchase>

    @Insert(onConflict = REPLACE)
    fun insert(purchase: Purchase)

    @Delete
    fun deletePurchase(purchase: Purchase)

    @Query("DELETE FROM purchase")
    fun deleteAll()

    @Query("SELECT * FROM purchase WHERE purchase.categoryId =:categoryId")
    fun getPurchasesByCategoryId(categoryId: Long): LiveData<List<Purchase>>

    @Query("SELECT * FROM purchase WHERE purchase.accountId =:accountId")
    fun getPurchasesByAccountId(accountId: Long): LiveData<List<Purchase>>
}
