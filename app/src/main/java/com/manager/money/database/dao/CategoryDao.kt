package com.manager.money.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.manager.money.database.data.Category

@Dao
interface CategoryDao {

    @Query("SELECT * FROM category")
    fun getAll(): List<Category>

    @Query("SELECT * FROM category WHERE category.id =:categoryDataId")
    fun getCategoryById(categoryDataId: Long): Category

    @Insert(onConflict = REPLACE)
    fun insert(categoryData: Category)

    @Delete
    fun deleteCategory(categoryData: Category)

    @Query("DELETE FROM category")
    fun deleteAll()

    @Query("SELECT * FROM category WHERE category.iconName =:categoryIconName")
    fun getCategoryIconByIconName(categoryIconName : String) : Category
}
