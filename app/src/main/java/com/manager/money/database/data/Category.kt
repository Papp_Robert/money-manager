package com.manager.money.database.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 * The category representation in the database.
 * Name of the category,
 * budget is the amount the user defines as a goal not to overspend,
 * icon is the selected icon for the category.
 *
 * Example = "Books", "book", 30
 */

@Entity(tableName = "Category")
data class Category(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "name")
        var name: String,
        @ColumnInfo(name = "iconName")
        var iconName: String,
        @ColumnInfo(name = "budget")
        var budget: Long = 0,
        @ColumnInfo(name = "isExpense")
        var isExpense: Boolean = false
) {
    @Ignore
    constructor(name: String = "", iconName: String = "", budget: Long = 0, isExpense: Boolean = false) : this(0, name, iconName, budget, isExpense)
}

