package com.manager.money.database.data

import androidx.room.*
import com.manager.money.database.dao.data.Account
import com.manager.money.utils.TimeStampConverter
import java.util.*

@Entity(tableName = "Purchase",
        foreignKeys = [
                ForeignKey(entity = Category::class,
                        parentColumns = ["id"],
                        childColumns = ["categoryId"],
                        onDelete = ForeignKey.NO_ACTION,
                        onUpdate = ForeignKey.NO_ACTION),
                ForeignKey(entity = Account::class,
                        parentColumns = ["id"],
                        childColumns = ["accountId"],
                        onDelete = ForeignKey.NO_ACTION,
                        onUpdate = ForeignKey.NO_ACTION)
        ])
@TypeConverters(TimeStampConverter::class)
data class Purchase(
        @PrimaryKey(autoGenerate = true)
        var id: Long?,

        @ColumnInfo(name = "categoryId")
        var categoryId: Long,

        @ColumnInfo(name = "accountId")
        var accountId: Long,

        @ColumnInfo(name = "value")
        var value: Double,

        @ColumnInfo(name = "purchaseDate")
        var purchaseDate: Date?,

        @ColumnInfo(name = "note")
        var note: String,

        @ColumnInfo(name = "isRecurring")
        var isRecurring: Boolean,

        @ColumnInfo(name = "startDate")
        var startDate: Date?,

        @ColumnInfo(name = "repeatTime")
        var repeatTime: Date?) {
        constructor() : this (null, 0, 0, 0.0, Date(), "", false, null, null)

}
