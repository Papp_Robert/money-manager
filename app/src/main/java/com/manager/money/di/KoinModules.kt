package com.manager.money.di

import androidx.room.Room
import com.manager.money.database.databases.AppDatabase
import com.manager.money.repositories.implementations.AccountRepository
import com.manager.money.repositories.implementations.CategoryRepository
import com.manager.money.repositories.implementations.PurchaseRepository
import com.manager.money.repositories.interfaces.IAccountRepository
import com.manager.money.repositories.interfaces.ICategoryRepository
import com.manager.money.repositories.interfaces.IPurchaseRepository
import com.manager.money.viewModels.ChooseCategoriesFromAllPossibleCategoriesViewModel
import com.manager.money.viewModels.ChooseCategoryViewModel
import com.manager.money.viewModels.HomePageViewModel
import com.manager.money.viewModels.NewExpenseViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val myModule = module(override = true) {

    //Account repository
    single { AccountRepository(get()) as IAccountRepository }

    //Purchase repository
    single { PurchaseRepository(get()) as IPurchaseRepository }

    //Category repository
    single { CategoryRepository(get()) as ICategoryRepository }

    // Room Database instance
    single {
        Room.databaseBuilder(
                get(),
                AppDatabase::class.java, "moneyManager.db")
                .build()
    }
    //AccountDao instance (get instance from AccountDatabase)
    single { get<AppDatabase>().accountDao() }

    //CategoryDao instance (get instance from AccountDatabase)
    single { get<AppDatabase>().categoryDao() }

    //PurchaseDao instance (get instance from AccountDatabase)
    single { get<AppDatabase>().purchaseDao() }

    //HomePageVM
    viewModel { HomePageViewModel(get()) }

    //ChooseCategoryVM
    viewModel { ChooseCategoryViewModel(get()) }

    //NewExpenseVM
    viewModel { NewExpenseViewModel(get(), get()) }

    //ChooseCategoriesFromAllPossibleCategoriesVM
    viewModel { ChooseCategoriesFromAllPossibleCategoriesViewModel(get()) }
}
