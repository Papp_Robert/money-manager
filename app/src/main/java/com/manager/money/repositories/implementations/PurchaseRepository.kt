package com.manager.money.repositories.implementations

import androidx.lifecycle.LiveData
import com.manager.money.database.dao.PurchaseDao
import com.manager.money.database.data.Purchase
import com.manager.money.repositories.interfaces.IPurchaseRepository
import io.reactivex.Completable

class PurchaseRepository(private val purchaseDao: PurchaseDao) : IPurchaseRepository {
    override fun getAll(): LiveData<List<Purchase>> {
        return purchaseDao.getAll()
    }

    override fun getPurchaseById(purchaseId: Long): LiveData<Purchase> {
        return purchaseDao.getPurchaseById(purchaseId)
    }

    override fun insert(purchase: Purchase): Completable {
        return Completable.create { observer ->
            purchaseDao.insert(purchase)
            observer.onComplete()
        }
    }

    override fun deletePurchase(purchase: Purchase) {
        purchaseDao.deletePurchase(purchase)
    }

    override fun deleteAll() {
        purchaseDao.deleteAll()
    }

    override fun getPurchasesByCategoryId(categoryId: Long): LiveData<List<Purchase>> {
        return purchaseDao.getPurchasesByCategoryId(categoryId)
    }

    override fun getPurchasesByAccountId(accountId: Long): LiveData<List<Purchase>> {
        return purchaseDao.getPurchasesByAccountId(accountId)
    }
}
