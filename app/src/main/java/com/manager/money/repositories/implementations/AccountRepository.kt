package com.manager.money.repositories.implementations

import androidx.lifecycle.LiveData
import com.manager.money.database.dao.AccountDao
import com.manager.money.database.dao.data.Account
import com.manager.money.repositories.interfaces.IAccountRepository
import io.reactivex.Completable

class AccountRepository(private val accountDao: AccountDao) : IAccountRepository {

    override fun deleteAll() {
        accountDao.deleteAll()
    }

    override fun getAccoundById(accountId: Long): LiveData<Account> {
        return accountDao.getAccountById(accountId)
    }

    override fun insert(account: Account): Completable {
        return Completable.create { observer ->
            accountDao.insert(account)
            observer.onComplete()
        }
    }

    override fun getAll(): LiveData<List<Account>> {
        return accountDao.getAll()
    }
}