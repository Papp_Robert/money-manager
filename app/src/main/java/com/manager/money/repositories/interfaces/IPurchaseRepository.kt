package com.manager.money.repositories.interfaces

import androidx.lifecycle.LiveData
import com.manager.money.database.data.Purchase
import io.reactivex.Completable

interface IPurchaseRepository {
    fun getAll(): LiveData<List<Purchase>>

    fun getPurchaseById(purchaseId: Long): LiveData<Purchase>

    fun insert(purchase: Purchase): Completable

    fun deletePurchase(purchase: Purchase)

    fun deleteAll()

    fun getPurchasesByCategoryId(categoryId: Long): LiveData<List<Purchase>>

    fun getPurchasesByAccountId(accountId: Long): LiveData<List<Purchase>>
}
