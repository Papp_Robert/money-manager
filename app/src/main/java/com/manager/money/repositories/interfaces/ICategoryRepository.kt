package com.manager.money.repositories.interfaces

import com.manager.money.database.data.Category
import io.reactivex.Completable
import io.reactivex.Single

interface ICategoryRepository {
    fun getAll(): Single<List<Category>>

    fun getCategoryById(categoryDataId: Long): Single<Category>

    fun insert(categoryData: Category): Completable

    fun deleteCategory(categoryData: Category)

    fun deleteAll(): Completable

    fun getCategoryByIconName(categoryIconName: String): Single<Category>
}