package com.manager.money.repositories.implementations

import com.manager.money.database.dao.CategoryDao
import com.manager.money.database.data.Category
import com.manager.money.repositories.interfaces.ICategoryRepository
import io.reactivex.Completable
import io.reactivex.Single

class CategoryRepository(private val categoryDao: CategoryDao) : ICategoryRepository {

    override fun getCategoryById(categoryDataId: Long): Single<Category> {
        return Single.create { observer ->
            val category = categoryDao.getCategoryById(categoryDataId)
            observer.onSuccess(category)
        }
    }

    override fun insert(categoryData: Category): Completable {
        return Completable.create { observer ->
            categoryDao.insert(categoryData)
            observer.onComplete()
        }
    }

    override fun deleteCategory(categoryData: Category) {
        categoryDao.deleteCategory(categoryData)
    }

    override fun deleteAll() : Completable{
        return Completable.create { observer ->
            categoryDao.deleteAll()
            observer.onComplete()
        }
    }

    override fun getAll(): Single<List<Category>> {
        return Single.create{observer->
            observer.onSuccess(categoryDao.getAll())
        }
    }

    override fun getCategoryByIconName(categoryIconName: String): Single<Category> {
        return Single.create { observer ->
            val foundedCategory = categoryDao.getCategoryIconByIconName(categoryIconName)
            observer.onSuccess(foundedCategory)
        }
    }
}