package com.manager.money.repositories.interfaces

import androidx.lifecycle.LiveData
import com.manager.money.database.dao.data.Account
import io.reactivex.Completable

interface IAccountRepository {
    fun getAll() : LiveData<List<Account>>

    fun getAccoundById(accountId: Long) : LiveData<Account>

    fun insert(account: Account) : Completable

    fun deleteAll()
}