package com.manager.money.viewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.manager.money.moneymanager.R
import com.manager.money.renderers.MaterialTextView

class PurchaseListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var categoryIconTextView: MaterialTextView = view.findViewById(R.id.categoryIcon)
    var currencyTextView: TextView = view.findViewById(R.id.currencyTextView)
    var noteTextField: TextView = view.findViewById(R.id.noteTextView)
    var valueTextField: TextView = view.findViewById(R.id.valueTextView)
}
