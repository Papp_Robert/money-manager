package com.manager.money.moneymanager

import android.app.Application
import com.manager.money.di.myModule
import org.koin.android.ext.android.startKoin

class MoneyManagerApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(applicationContext, listOf(myModule))
    }

}