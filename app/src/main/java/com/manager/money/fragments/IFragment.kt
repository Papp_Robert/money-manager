package com.manager.money.fragments

import android.content.Context

interface IFragment {

    fun getContext() : Context
}