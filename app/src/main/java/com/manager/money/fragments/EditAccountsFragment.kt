package com.manager.money.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentEditAccountsScreenBinding

class EditAccountsFragment : Fragment() {
    private lateinit var binding: FragmentEditAccountsScreenBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =

            DataBindingUtil.inflate<FragmentEditAccountsScreenBinding>(layoutInflater, R.layout.fragment_edit_accounts_screen, container, false)
                    .also {
                binding = it
            }.root

}
