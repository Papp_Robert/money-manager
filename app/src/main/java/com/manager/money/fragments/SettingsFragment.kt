package com.manager.money.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentSettingsScreenBinding

class SettingsFragment : Fragment() {
    private lateinit var binding: FragmentSettingsScreenBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            DataBindingUtil.inflate<FragmentSettingsScreenBinding>(layoutInflater, R.layout.fragment_settings_screen, container, false).also {
                binding = it
            }.root

}
