package com.manager.money.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentEditCategoriesScreenBinding

class EditCategoriesFragment : Fragment() {
    private lateinit var binding: FragmentEditCategoriesScreenBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            DataBindingUtil.inflate<FragmentEditCategoriesScreenBinding>(layoutInflater, R.layout.fragment_edit_categories_screen, container, false).also {
                binding = it
            }.root

}
