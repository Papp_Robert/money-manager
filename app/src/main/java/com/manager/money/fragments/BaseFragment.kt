package com.manager.money.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.manager.money.activities.HomeActivity
import com.manager.money.moneymanager.R
import kotlin.reflect.KClass


abstract class BaseFragment<TBinding : ViewDataBinding>(
        val bindingClass: KClass<TBinding>,
        val layout: Int) : Fragment(), IFragment {

    lateinit var navController: NavController
    lateinit var viewDataBinding: TBinding
    lateinit var homeActivity: HomeActivity
    lateinit var fragmentContext: IFragment

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        homeActivity = activity as HomeActivity
        navController = Navigation.findNavController(homeActivity, R.id.my_nav_host_fragment)
        fragmentContext = this

        setViewModel()
    }

    abstract fun setViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            DataBindingUtil.inflate<TBinding>(layoutInflater, layout, container, false).also {
                viewDataBinding = it
            }.root


    override fun getContext(): Context {
        return activity?.applicationContext!!
    }
}

