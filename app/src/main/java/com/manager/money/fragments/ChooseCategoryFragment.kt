package com.manager.money.fragments

import android.os.Bundle
import androidx.lifecycle.Observer
import com.manager.money.constants.Constants
import com.manager.money.extensions.observeOnce
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentChooseCategoryScreenBinding
import com.manager.money.viewModels.ChooseCategoryViewModel
import kotlinx.android.synthetic.main.fragment_choose_category_screen.*
import org.koin.android.ext.android.inject

class ChooseCategoryFragment : BaseFragment<FragmentChooseCategoryScreenBinding>
(FragmentChooseCategoryScreenBinding::class,
        R.layout.fragment_choose_category_screen) {

    private val viewModel : ChooseCategoryViewModel by inject()

    private val observer = Observer<String?> {
        val bundle = Bundle()
        bundle.putString(Constants.chosenCategoryKeyName, it)

        navController.navigate(R.id.action_chooseCategoryScreen_to_newExpenseScreen, bundle)
    }

    override fun setViewModel() {
        viewDataBinding.viewModel = viewModel
    }

    override fun onStart() {
        super.onStart()

        viewModel.fragment = fragmentContext
        viewModel.setupRecyclerView(category_selection_list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.selectedCategory.observeOnce(this, observer)
    }
}