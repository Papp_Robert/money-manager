package com.manager.money.fragments

import androidx.lifecycle.Observer
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentChooseCategoriesFromAllPossibleCategoriesScreenBinding
import com.manager.money.viewModels.ChooseCategoriesFromAllPossibleCategoriesViewModel
import kotlinx.android.synthetic.main.fragment_choose_categories_from_all_possible_categories_screen.*
import org.koin.android.ext.android.inject

class ChooseCategoriesFromAllPossibleCategoriesFragment :
        BaseFragment<FragmentChooseCategoriesFromAllPossibleCategoriesScreenBinding>
        (FragmentChooseCategoriesFromAllPossibleCategoriesScreenBinding::class, R.layout.fragment_choose_categories_from_all_possible_categories_screen) {


    private val viewModel: ChooseCategoriesFromAllPossibleCategoriesViewModel by inject()

    override fun setViewModel() {
        viewDataBinding.viewModel = viewModel

        viewModel.saveFinished.observe(this, Observer {
            navController.navigate(R.id.action_chooseCategoriesFromAllPossibleCategoriesScreen_to_homeScreen)
        })
    }

    override fun onStart() {
        super.onStart()

        viewModel.fragment = fragmentContext
        viewModel.setupRecyclerView(category_selection_from_all_categories_list)
    }
}