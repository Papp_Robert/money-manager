package com.manager.money.fragments

import android.os.Bundle
import com.manager.money.constants.Constants
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentNewExpenseScreenBinding
import com.manager.money.viewModels.NewExpenseViewModel
import kotlinx.android.synthetic.main.fragment_new_expense_screen.*
import org.koin.android.ext.android.inject


class NewExpenseFragment : BaseFragment<FragmentNewExpenseScreenBinding>
(FragmentNewExpenseScreenBinding::class,
        R.layout.fragment_new_expense_screen) {

    private val viewModel : NewExpenseViewModel by inject()

    override fun setViewModel() {
        viewDataBinding.viewModel = viewModel
        viewModel.activity = homeActivity

        viewDataBinding.setLifecycleOwner(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.choosenCategoryIconName = arguments?.get(Constants.chosenCategoryKeyName).toString()

        addNewExpenseButton.setOnClickListener {
            viewModel.saveExpenseToRepository()
            navController.navigate(R.id.action_newExpenseScreen_to_homeScreen)
        }

        viewModel.actualDateInString.value = viewModel.initActualDate()
    }
}