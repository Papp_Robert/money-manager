package com.manager.money.fragments


import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.FragmentHomeScreenBinding
import com.manager.money.repositories.interfaces.ICategoryRepository
import com.manager.money.viewModels.HomePageViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home_screen.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.android.ext.android.inject

private const val animationDurationInMs = 250L
private const val mainFabRotation = 45f

class HomeFragment : BaseFragment<FragmentHomeScreenBinding>
(FragmentHomeScreenBinding::class, R.layout.fragment_home_screen) {

    private var isFabMenuVisible = false
    private val viewModel: HomePageViewModel by inject()
    private val categoryRepository: ICategoryRepository by inject()

    override fun onStart() {
        super.onStart()

        (activity as AppCompatActivity).setSupportActionBar(bottom_app_bar)

        viewModel.fragment = fragmentContext

        categoryRepository.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(it.isEmpty()) {
                       navController.navigate(R.id.action_homeScreen_to_chooseFromAllPossibleCategoriesToSaveToDatabase)
                    }
                },{
                    //TODO: ADD log

                })

        fab.onClick {
            startFabMenuAnimation()
            negateFabMenuVisibility()
        }

        fabNewExpense.onClick {
            navController.navigate(R.id.action_homeScreen_to_chooseCategorySelectionScreen)
            isFabMenuVisible = false
        }
        fabNewExpense.isEnabled = false
        fabNewIncome.isEnabled = false

        viewModel.setupRecyclerView(purchases_list)
    }

    override fun setViewModel() {
        viewDataBinding.viewModel = viewModel
    }

    private fun startFabMenuAnimation() {
        if (isFabMenuVisible) {
            animateFabMenu(-mainFabRotation, 0f)
        } else {
            animateFabMenu(mainFabRotation, 1f)
        }
    }

    private fun animateFabMenu(rotateMainFabBy: Float, scaleMenuItems: Float) {
        fab.animate()
                .setInterpolator(LinearInterpolator())
                .rotationBy(rotateMainFabBy)
                .setDuration(animationDurationInMs)
                .start()

        fabNewExpense.animate()
                .setInterpolator(LinearInterpolator())
                .scaleX(scaleMenuItems)
                .scaleY(scaleMenuItems)
                .setDuration(animationDurationInMs)
                .start()

        fabNewIncome.animate()
                .setInterpolator(LinearInterpolator())
                .scaleX(scaleMenuItems)
                .scaleY(scaleMenuItems)
                .setDuration(animationDurationInMs)
                .start()
    }

    private fun negateFabMenuVisibility() {
        isFabMenuVisible = !isFabMenuVisible
        fabNewExpense.isEnabled = !fabNewExpense.isEnabled
        fabNewIncome.isEnabled = !fabNewIncome.isEnabled
    }
}
