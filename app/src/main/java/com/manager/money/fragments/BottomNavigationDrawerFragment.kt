package com.manager.money.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.manager.money.moneymanager.R
import kotlinx.android.synthetic.main.fragment_bottomsheet.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class BottomNavigationDrawerFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bottomsheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = Navigation.findNavController(activity!!, R.id.my_nav_host_fragment)

        editAccountsNavigation.onClick {
            navController.navigate(R.id.action_bottom_drawer_to_edit_accounts)
            dismiss()
        }

        editCategoriesNavigation.onClick {
            navController.navigate(R.id.action_bottom_drawer_to_edit_categories)
            dismiss()
        }

        settingsNavigation.onClick {
            navController.navigate(R.id.action_bottom_drawer_to_settings)
            dismiss()
        }

        statisticsNavigation.onClick {
            navController.navigate(R.id.action_bottom_drawer_to_statistics)
            dismiss()
        }
    }
}
