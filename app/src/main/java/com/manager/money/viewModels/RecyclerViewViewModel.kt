package com.manager.money.viewModels

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.manager.money.fragments.IFragment

abstract class RecyclerViewViewModel : ViewModel() {

    var recyclerView: RecyclerView? = null

    lateinit var fragment: IFragment

    abstract fun createAdapter()

    abstract fun createLayoutManager()

    fun setupRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView

        createLayoutManager()
        createAdapter()
    }
}