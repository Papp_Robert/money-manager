package com.manager.money.viewModels

import android.app.DatePickerDialog
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.manager.money.activities.HomeActivity
import com.manager.money.database.data.Purchase
import com.manager.money.extensions.toString
import com.manager.money.repositories.interfaces.ICategoryRepository
import com.manager.money.repositories.interfaces.IPurchaseRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class NewExpenseViewModel(private val purchaseRepository: IPurchaseRepository, private val categoryRepository: ICategoryRepository) : ViewModel() {

    var purchase: Purchase = Purchase()
    private val dateFormat = "MMMM dd, yyyy"

    lateinit var activity: HomeActivity

    var actualDateInString: MutableLiveData<String> = MutableLiveData()

    lateinit var choosenCategoryIconName: String

    fun getDate(): Date {
        purchase.purchaseDate = Calendar.getInstance().time
        return purchase.purchaseDate!!
    }

    fun initActualDate(): String {
        return getDate().toString(dateFormat)
    }

    fun saveExpenseToRepository() {
        categoryRepository.getCategoryByIconName(choosenCategoryIconName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    purchase.categoryId = it.id!!
                    purchaseRepository.insert(purchase).
                            subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({},{
                                /*TODO ADD LOG(Timber?)*/ })

                }, {
                    //TODO: ADD LOG
                })
    }

    fun clickDataPicker() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            actualDateInString.postValue(calendar.time.toString(dateFormat))
        }, year, month, day)
        datePickerDialog.show()
    }
}