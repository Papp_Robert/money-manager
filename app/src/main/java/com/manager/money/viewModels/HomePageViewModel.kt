package com.manager.money.viewModels


import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nitrico.lastadapter.BR
import com.github.nitrico.lastadapter.LastAdapter
import com.manager.money.database.data.Purchase
import com.manager.money.moneymanager.R
import com.manager.money.repositories.interfaces.IPurchaseRepository
import org.jetbrains.anko.toast
import java.util.*


class HomePageViewModel(private val purchaseRepository: IPurchaseRepository?) : RecyclerViewViewModel() {

    override fun createLayoutManager() {
        recyclerView?.layoutManager = LinearLayoutManager(fragment.getContext())
    }

    override fun createAdapter() {
        val purchases = listOf(
                Purchase(
                        0,
                        2,
                        2,
                        45.0,
                        Date(),
                        "bread",
                        false,
                        null,
                        null
                ),
                Purchase(
                        0,
                        2,
                        2,
                        5.0,
                        Date(),
                        "beer",
                        false,
                        null,
                        null
                ),
                Purchase(
                        0,
                        1,
                        2,
                        2.0,
                        Date(),
                        "chips",
                        false,
                        null,
                        null
                )
        )

        //TODO : Add just the purchaseRepo.GetAll method
        recyclerView?.adapter = LastAdapter(list = purchases, variable = BR.item)
                .map<Purchase, ViewDataBinding>(R.layout.expense_list_item){
                    onClick { fragment.getContext().toast("BLABLA") }
                }
                .into(recyclerView!!)
    }

    fun filterInPurchases() {
    }
}
