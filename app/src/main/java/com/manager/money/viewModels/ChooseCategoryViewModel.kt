package com.manager.money.viewModels

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import com.github.nitrico.lastadapter.BR
import com.github.nitrico.lastadapter.LastAdapter
import com.manager.money.moneymanager.R
import com.manager.money.moneymanager.databinding.ItemIconBindingImpl
import com.manager.money.repositories.interfaces.ICategoryRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChooseCategoryViewModel(private val categoryRepository: ICategoryRepository) : RecyclerViewViewModel() {
    companion object {
        private const val spanCount: Int = 5
    }

    var selectedCategory = MutableLiveData<String>()

    override fun createLayoutManager() {
        recyclerView?.layoutManager = GridLayoutManager(fragment.getContext(), spanCount)
    }

    override fun createAdapter() {
        categoryRepository.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val savedIcons = mutableListOf<String>()
                    for(icon in it){
                        savedIcons.add(icon.iconName)
                    }

            LastAdapter(savedIcons, BR.item)
                    .map<String, ViewDataBinding>(R.layout.item_icon) {

                        onClick {
                            selectedCategory.postValue((it.binding as ItemIconBindingImpl).item)
                        }

                    }
                    .into(recyclerView!!)
        },{})


    }
}