package com.manager.money.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import com.manager.money.adapters.CategoriesAdapter
import com.manager.money.helpers.Categories
import com.manager.money.repositories.interfaces.ICategoryRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChooseCategoriesFromAllPossibleCategoriesViewModel(private val categoryRepository: ICategoryRepository) : RecyclerViewViewModel() {
    companion object {
        const val spanCount: Int = 5
    }

    private var categoriesAdapter: CategoriesAdapter? = null
    var saveFinished: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    override fun createLayoutManager() {
        val gridLayoutManager = GridLayoutManager(fragment.getContext(), spanCount)
        val spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                when (position) {
                    0, 21, 37, 53, 64 -> return spanCount
                    else -> return 1
                }
            }
        }
        gridLayoutManager.spanSizeLookup = spanSizeLookup
        recyclerView!!.layoutManager = gridLayoutManager
    }

    override fun createAdapter() {
        val categories = Categories(fragment.getContext())
        categoriesAdapter = CategoriesAdapter(categories.allCategoryIcons, fragment.getContext())

        recyclerView!!.adapter = categoriesAdapter
    }

    fun saveCategoriesToRepository() {
        for (category in categoriesAdapter?.selectedCategories!!) {
            categoryRepository.insert(category)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        saveFinished.postValue(true)
                    }, {

                    })
        }
    }

}