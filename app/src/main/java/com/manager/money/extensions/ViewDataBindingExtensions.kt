package com.manager.money.extensions

import androidx.databinding.ViewDataBinding

fun ViewDataBinding.setVM(viewModel: Any) = this::class.java.methods.filter { it.name == "setViewmodel" }[0].invoke(this, viewModel)