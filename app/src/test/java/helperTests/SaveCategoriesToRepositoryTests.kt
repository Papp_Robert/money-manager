package helperTests

import android.content.Context
import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.manager.money.helpers.SaveCategoriesToRepository
import com.manager.money.moneymanager.R
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.*
import viewModelTests.mocks.MockCategoryRepository

open class SaveCategoriesToRepositoryTests {

    private lateinit var saveCategoriesToRepository: SaveCategoriesToRepository

    @Mock
    protected lateinit var mockContext: Context

    @Mock
    private lateinit var mockContextResources: Resources

    private val entertainmentIcons = mutableListOf("book", "local_cafe", "laptop")
    private val sportIcons = mutableListOf("golf_course", "fitness_center", "directions_walk", "directions_bike")
    private val travelIcons = mutableListOf("directions_bus", "tram", "map")
    private val shoppingIcons = mutableListOf("cake")
    private val miscIcons = mutableListOf<String>()

    private val mockCategoryRepository = MockCategoryRepository()

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    @Before
    open fun setUp() {
        mockContext = mock(Context::class.java)
        mockContextResources = mock(Resources::class.java)

        `when`(mockContext.resources).thenReturn(mockContextResources)

        `when`(mockContext.resources.getStringArray(R.array.category_icons_entertainment)).thenReturn(entertainmentIcons.toTypedArray())
        `when`(mockContext.resources.getStringArray(R.array.category_icons_misc)).thenReturn(miscIcons.toTypedArray())
        `when`(mockContext.resources.getStringArray(R.array.category_icons_shopping)).thenReturn(shoppingIcons.toTypedArray())
        `when`(mockContext.resources.getStringArray(R.array.category_icons_sports)).thenReturn(sportIcons.toTypedArray())
        `when`(mockContext.resources.getStringArray(R.array.category_icons_travel)).thenReturn(travelIcons.toTypedArray())

        saveCategoriesToRepository = SaveCategoriesToRepository(mockCategoryRepository, mockContext)

        initForRxTest()
    }

    @Test
    fun saveMiscCategoryIcons_CallsCategoryRepository_Successfully() {
        saveCategoriesToRepository.saveAllCategoriesToRepository()
        val categories = mockCategoryRepository.getAll()

        categories.subscribe({
            Assert.assertEquals(it.count(), 11)
        },{})
    }

    private fun initForRxTest() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }
}