package helperTests

import com.manager.money.helpers.Categories
import com.manager.money.models.CategoryType
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CategoriesTests : SaveCategoriesToRepositoryTests() {
    private lateinit var categories: Categories

    @Before
    override fun setUp() {
        super.setUp()
        categories = Categories(mockContext)
    }

    @Test
    fun entertainmentCategoryIcons_FirstElement_IsHeaderType() {
        Assert.assertEquals(categories.entertainmentCategoryIcons[0].safeCategoryType, CategoryType.HEADER)
    }

    @Test
    fun entertainmentCategoryIcons_ElementSize_DividedByFiveIsOneBecauseOfTheHeaderElement() {
        Assert.assertEquals(categories.entertainmentCategoryIcons.count() % 5, 1)
    }

    @Test
    fun entertainmentCategoryIcons_LastElementText_IsZero() {
        Assert.assertEquals(categories.entertainmentCategoryIcons.last().safeText, "")
    }

    @Test
    fun miscCategoryIcons_FirstElement_IsHeaderType() {
        Assert.assertEquals(categories.miscCategoryIcons[0].safeCategoryType, CategoryType.HEADER)
    }

    @Test
    fun miscCategoryIcons_ElementSize_DividedByFiveIsOneBecauseOfTheHeaderElement() {
        Assert.assertEquals(categories.miscCategoryIcons.count() % 5, 1)
    }

    @Test
    fun miscCategoryIcons_HasOnlyOneElement_WhichIsTheHeader() {
        Assert.assertEquals(categories.miscCategoryIcons.last().safeText, "MISC")
    }

    @Test
    fun shoppingCategoryIcons_FirstElement_IsHeaderType() {
        Assert.assertEquals(categories.shoppingCategoryIcons[0].safeCategoryType, CategoryType.HEADER)
    }

    @Test
    fun shoppingCategoryIcons_ElementSize_DividedByFiveIsOneBecauseOfTheHeaderElement() {
        Assert.assertEquals(categories.shoppingCategoryIcons.count() % 5, 1)
    }

    @Test
    fun shoppingCategoryIcons_LastElementText_IsZero() {
        Assert.assertEquals(categories.shoppingCategoryIcons.last().safeText, "")
    }


    @Test
    fun travelCategoryIcons_FirstElement_IsHeaderType() {
        Assert.assertEquals(categories.travelCategoryIcons[0].safeCategoryType, CategoryType.HEADER)
    }

    @Test
    fun travelCategoryIcons_ElementSize_DividedByFiveIsOneBecauseOfTheHeaderElement() {
        Assert.assertEquals(categories.travelCategoryIcons.count() % 5, 1)
    }

    @Test
    fun travelCategoryIcons_LastElementText_IsZero() {
        Assert.assertEquals(categories.travelCategoryIcons.last().safeText, "")
    }
}