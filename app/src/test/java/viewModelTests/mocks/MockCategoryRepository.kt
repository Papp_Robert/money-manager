package viewModelTests.mocks

import com.manager.money.database.data.Category
import com.manager.money.repositories.interfaces.ICategoryRepository
import io.reactivex.Completable
import io.reactivex.Single
import java.lang.Exception

class MockCategoryRepository : ICategoryRepository {

    private val categories = mutableListOf<Category>()

    override fun getAll(): Single<List<Category>> {
        return Single.create {observer->
            val categories = categories.filter { _ -> true }
            observer.onSuccess(categories)
        }
    }

    override fun getCategoryById(categoryDataId: Long): Single<Category> {
        return Single.create {observer ->
            try {
                val category = categories.find { c -> c.id == categoryDataId }
                observer.onSuccess(category!!)
            }
            catch (e: Exception){
                observer.onError(e)
            }
        }
    }

    override fun insert(categoryData: Category) : Completable {
        return Completable.create {observer ->
            categories.add(categoryData)
            observer.onComplete()
        }
    }

    override fun deleteCategory(categoryData: Category) {
        categories.remove(categoryData)
    }

    override fun deleteAll() : Completable {
        return Completable.create {observer->
            categories.removeAll { _ -> true }
            observer.onComplete()
        }
    }

    override fun getCategoryByIconName(categoryIconName: String): Single<Category> {
        return Single.create {observer->
            try {
                val category = categories.find { c -> c.name == categoryIconName }
                observer.onSuccess(category!!)
            }
            catch (e: Exception){
                observer.onError(e)
            }
        }
    }
}