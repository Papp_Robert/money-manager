package viewModelTests.mocks

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.manager.money.database.data.Purchase
import com.manager.money.repositories.interfaces.IPurchaseRepository
import io.reactivex.Completable

class MockPurchaseRepository : IPurchaseRepository {
    private val purchases = mutableListOf<Purchase>()


    override fun getAll(): LiveData<List<Purchase>> {
        val purchaseLiveData = MutableLiveData<List<Purchase>>()

        purchaseLiveData.value = purchases.filter { _ -> true }
        return purchaseLiveData
    }

    override fun getPurchaseById(purchaseId: Long): LiveData<Purchase> {
        val purchaseLiveData = MutableLiveData<Purchase>()

        purchaseLiveData.value = purchases.find { x -> x.id == purchaseId }
        return purchaseLiveData
    }

    override fun insert(purchase: Purchase): Completable {
        return Completable.create { observer ->
            purchases.add(purchase)
            observer.onComplete()
        }
    }

    override fun deletePurchase(purchase: Purchase) {
        purchases.remove(purchase)
    }


    override fun deleteAll() {
        purchases.removeAll { _ -> true }
    }

    override fun getPurchasesByCategoryId(categoryId: Long): LiveData<List<Purchase>> {
        val purchaseLiveData = MutableLiveData<List<Purchase>>()

        purchaseLiveData.value = purchases.filter { x -> x.categoryId == categoryId }
        return purchaseLiveData
    }

    override fun getPurchasesByAccountId(accountId: Long): LiveData<List<Purchase>> {
        val purchaseLiveData = MutableLiveData<List<Purchase>>()

        purchaseLiveData.value = purchases.filter { x -> x.categoryId == accountId }
        return purchaseLiveData
    }
}