package viewModelTests

import com.manager.money.viewModels.NewExpenseViewModel
import org.junit.Assert
import org.junit.Test
import viewModelTests.mocks.MockCategoryRepository
import viewModelTests.mocks.MockPurchaseRepository
import java.util.*


class NewExpenseViewModelTests {

    private var newExpenseVM = NewExpenseViewModel(MockPurchaseRepository(), MockCategoryRepository())

    @Test
    fun newExpenseViewModel_GetDate_ShouldReturnTheSameYear() {
        val time = newExpenseVM.initActualDate()
        val date = Calendar.getInstance().get(Calendar.YEAR)

        Assert.assertEquals(date.toString(), time.takeLast(4))
    }
}