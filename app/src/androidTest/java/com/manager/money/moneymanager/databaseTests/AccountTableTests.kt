package com.manager.money.moneymanager.databaseTests

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.runner.AndroidJUnit4
import com.manager.money.database.dao.AccountDao
import com.manager.money.database.dao.data.Account
import com.manager.money.database.databases.AppDatabase
import com.manager.money.moneymanager.di.roomTestModule
import org.junit.*
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest

@RunWith(AndroidJUnit4::class)
class AccountTableTests : KoinTest, BaseTableTests() {

    private val appDatabase: AppDatabase by inject()
    private val accountDao: AccountDao by inject()

    private var firstAccount : Account? = null
    private var secondAccount: Account? = null

    //TODO: If live data error will be fixed by android this is not needed(androidTestImplementation "androidx.arch.core:core-testing:2.0.0-rc01")
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        loadKoinModules(listOf(roomTestModule))
        firstAccount = Account(0, "KP", "Huf", "Cash", "First icon")
        secondAccount = Account(1, "Card", "Euro", "bank account", "Second icon")
    }

    @After
    fun tearDown() {
        appDatabase.close()
        stopKoin()
        firstAccount = null
        secondAccount = null
    }

    @Test
    fun insertAccount_ToDatabase_Successful() {
        accountDao.insert(firstAccount!!)

        val accountTest = getValue(accountDao.getAccountById(firstAccount?.id!!))

        Assert.assertEquals(accountTest.name, "KP")
    }

    @Test
    fun insertMoreAccounts_ToDatabase_Successful() {
        accountDao.insert(firstAccount!!)
        accountDao.insert(secondAccount!!)

        val accounts = getValue(accountDao.getAll())

        Assert.assertEquals(accounts.size, 2)
        Assert.assertEquals(accounts[0].name, "KP")
        Assert.assertEquals(accounts[1].iconName, "Second icon")
    }

    @Test
    fun insertMoreAccounts_ToDatabase_ReplaceBecauseOfSameId() {
        accountDao.insert(firstAccount!!)

        secondAccount?.id = 0

        accountDao.insert(secondAccount!!)

        val accounts = getValue(accountDao.getAll())

        Assert.assertEquals(accounts.size, 1)
        Assert.assertEquals(accounts[0].name, "Card")
    }

    @Test
    fun insertMoreAccounts_ToDatabase_AfterDeleteAllEmpty() {
        accountDao.insert(firstAccount!!)
        accountDao.insert(secondAccount!!)

        var accounts = getValue(accountDao.getAll())

        Assert.assertEquals(accounts.size, 2)
        Assert.assertEquals(accounts[0].name, "KP")
        Assert.assertEquals(accounts[1].iconName, "Second icon")

        accountDao.deleteAll()
        accounts = getValue(accountDao.getAll())
        Assert.assertEquals(accounts.size, 0)
    }

    @Test
    fun insertAccount_ToDatabaseAndGetAccountById_GetsTheCorrectResult() {
        accountDao.insert(firstAccount!!)

        val account = getValue(accountDao.getAccountById(0))

        Assert.assertEquals(account.name, "KP")
        Assert.assertEquals(account.iconName, "First icon")
    }

    @Test
    fun insertAccount_ToDatabaseAndGetAccountById_GetsNullWithNonExistingId() {
        accountDao.insert(firstAccount!!)

        val account = getValue(accountDao.getAccountById(32))

        Assert.assertEquals(account, null)
    }
}
