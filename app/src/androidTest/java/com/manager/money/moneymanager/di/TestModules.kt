package com.manager.money.moneymanager.di

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import com.manager.money.database.databases.AppDatabase
import com.manager.money.repositories.implementations.AccountRepository
import com.manager.money.repositories.implementations.CategoryRepository
import com.manager.money.repositories.implementations.PurchaseRepository
import com.manager.money.repositories.interfaces.IAccountRepository
import com.manager.money.repositories.interfaces.ICategoryRepository
import com.manager.money.repositories.interfaces.IPurchaseRepository
import org.koin.dsl.module.module

/**
 * In-Memory Room Database definition
 */
val roomTestModule = module(override = true) {

    //Account repository
    single { AccountRepository(get()) as IAccountRepository }

    //Purchase repository
    single { PurchaseRepository(get()) as IPurchaseRepository }

    //Category repository
    single { CategoryRepository(get()) as ICategoryRepository }

    single {
        // In-Memory database config
        Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AppDatabase::class.java)
                .allowMainThreadQueries()
                .build()
    }
    //AccountDao instance (get instance from database)
    single { get<AppDatabase>().categoryDao() }

    //AccountDao instance (get instance from database)
    single { get<AppDatabase>().accountDao() }

    //PurchaseDao instance (get instance from database)
    single { get<AppDatabase>().purchaseDao() }
}
