package com.manager.money.moneymanager.databaseTests

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.runner.AndroidJUnit4
import com.manager.money.database.dao.CategoryDao
import com.manager.money.database.data.Category
import com.manager.money.database.databases.AppDatabase
import com.manager.money.moneymanager.di.roomTestModule
import org.junit.*
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest

@RunWith(AndroidJUnit4::class)
class CategoryTableTests : KoinTest, BaseTableTests() {

    private val appDatabase: AppDatabase by inject()
    private val categoryDataDao: CategoryDao by inject()

    private var category: Category? = null
    private var category2: Category? = null

    //TODO: If live data error will be fixed by android this is not needed(androidTestImplementation "androidx.arch.core:core-testing:2.0.0-rc01")
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        loadKoinModules(listOf(roomTestModule))
        category = Category("Books", "book", 20)
        category2 = Category( "Clothing", "local_mall", 30)
    }

    @After
    fun tearDown() {
        appDatabase.close()
        stopKoin()

        category = null
        category2 = null
    }

    @Test
    fun insertCategory_ToDatabase_Successfull() {
        categoryDataDao.insert(category!!)
        category!!.id = 1
        val categoryTest = categoryDataDao.getCategoryById(category?.id!!)

        Assert.assertEquals(categoryTest, category)
    }

    @Test
    fun deleteAllCategories_FromDatabase_Successfull() {
        categoryDataDao.insert(category!!)
        category!!.id = 1

        categoryDataDao.insert(category2!!)
        category2!!.id = 2

        categoryDataDao.deleteAll()

        val categories = categoryDataDao.getAll()
        Assert.assertEquals(categories.size, 0)
    }

    @Test
    fun getCategoryById_FromDatabase_WithNonExistingId() {
        categoryDataDao.insert(category!!)
        category!!.id = 1

        val categoryFromDb = categoryDataDao.getCategoryById(100)

        Assert.assertEquals(categoryFromDb, null)
    }

    @Test
    fun getCategoryById_FromDatabase_Successful() {
        categoryDataDao.insert(category!!)
        category!!.id = 1

        val categoryFromDb = categoryDataDao.getCategoryById(category?.id!!)

        Assert.assertEquals(categoryFromDb, category)
    }

    @Test
    fun deleteCategory_FromDatabase_Successful() {
        categoryDataDao.insert(category!!)
        category!!.id = 1

        var categoriesFromDb = categoryDataDao.getAll()

        Assert.assertEquals(categoriesFromDb[0], category)

        categoryDataDao.deleteCategory(category!!)

        categoriesFromDb = categoryDataDao.getAll()

        Assert.assertEquals(categoriesFromDb.size, 0)
    }

    @Test
    fun getCategoryByName_FromDatabase_Successful() {
        categoryDataDao.insert(category!!)
        category!!.id = 1

        val categoryFromDb = categoryDataDao.getCategoryIconByIconName("book")

        Assert.assertEquals(categoryFromDb, category)
    }
}
