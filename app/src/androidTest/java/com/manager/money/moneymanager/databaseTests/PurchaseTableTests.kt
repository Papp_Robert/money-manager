package com.manager.money.moneymanager.databaseTests

import android.database.sqlite.SQLiteConstraintException
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.manager.money.database.dao.AccountDao
import com.manager.money.database.dao.CategoryDao
import com.manager.money.database.dao.PurchaseDao
import com.manager.money.database.dao.data.Account
import com.manager.money.database.data.Category
import com.manager.money.database.data.Purchase
import com.manager.money.database.databases.AppDatabase
import com.manager.money.moneymanager.di.roomTestModule
import junit.framework.Assert.fail
import org.junit.*
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import java.util.*

class PurchaseTableTests : KoinTest, BaseTableTests() {

    private val appDatabase: AppDatabase by inject()
    private val accountDao: AccountDao by inject()
    private val categoryDao: CategoryDao by inject()
    private val purchaseDao: PurchaseDao by inject()

    private var account: Account? = null
    private var category: Category? = null

    //TODO: If live data error will be fixed by android this is not needed(androidTestImplementation "androidx.arch.core:core-testing:2.0.0-rc01")
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        loadKoinModules(listOf(roomTestModule))

        account = Account(0, "KP", "Ft", "Card", "book")
        category = Category("Books", "book", 20)

        categoryDao.insert(category!!)
        val instertedCategory = categoryDao.getCategoryIconByIconName(category!!.iconName)

        category!!.id = instertedCategory.id
    }

    @After
    fun tearDown() {
        appDatabase.close()
        stopKoin()

        account = null
        category = null
    }

    @Test
    fun insertPurchase_ToDatabase_Successful() {
        val purchase = Purchase(
                0,
                category?.id!!,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )

        accountDao.insert(account!!)
        purchaseDao.insert(purchase)

        val purchaseTest = getValue(purchaseDao.getPurchaseById(purchase.id!!))

        Assert.assertEquals(purchaseTest, purchase)
    }

    @Test
    fun deleteAllPurchases_FromDatabase_Successful() {
        val purchase = Purchase(
                0,
                category?.id!!,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        val purchase2 = Purchase(
                1,
                category?.id!!,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        accountDao.insert(account!!)
        purchaseDao.insert(purchase)
        purchaseDao.insert(purchase2)

        var purchases = getValue(purchaseDao.getAll())
        Assert.assertEquals(purchases.size, 2)

        purchaseDao.deleteAll()

        purchases = getValue(purchaseDao.getAll())

        Assert.assertEquals(purchases.size, 0)
    }

    @Test
    fun deletePurchase_FromDatabase_Successful() {
        val purchase = Purchase(
                0,
                category?.id!!,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        accountDao.insert(account!!)
        purchaseDao.insert(purchase)

        purchaseDao.deletePurchase(purchase)

        val purchases = getValue(purchaseDao.getAll())
        Assert.assertEquals(purchases.size, 0)
    }


    @Test
    fun getPurchases_FromDatabase_ByExistingCategoryId() {
        val purchase = Purchase(
                0,
                category?.id!!,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        accountDao.insert(account!!)
        purchaseDao.insert(purchase)

        val purchases = getValue(purchaseDao.getPurchasesByCategoryId(category?.id!!))

        Assert.assertEquals(purchases.size, 1)
        Assert.assertEquals(purchases[0].accountId, account?.id!!)
    }

    @Test
    fun getPurchases_FromDatabase_ByNotExistingCategoryId() {
        val purchase = Purchase(
                0,
                100,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        accountDao.insert(account!!)
        categoryDao.insert(category!!)


        try {
            purchaseDao.insert(purchase)
            fail("The category id is not valid this method should fail")
        }
        catch(e : SQLiteConstraintException){
            assert(e.localizedMessage.startsWith("FOREIGN KEY constraint failed", true))
        }
    }

    @Test
    fun getPurchases_FromDatabase_ByExistingAccountId() {
        val purchase = Purchase(
                0,
                category?.id!!,
                account?.id!!,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        accountDao.insert(account!!)
        purchaseDao.insert(purchase)

        val purchases = getValue(purchaseDao.getPurchasesByAccountId(account?.id!!))

        Assert.assertEquals(purchases.size, 1)
    }

    @Test
    fun getPurchases_FromDatabase_ByNonExistingAccountId() {
        val purchase = Purchase(
                0,
                category?.id!!,
                200,
                45.0,
                Date(),
                "bread",
                false,
                null,
                null
        )
        accountDao.insert(account!!)
        categoryDao.insert(category!!)

        try {
            purchaseDao.insert(purchase)
            fail("The account id is not valid this method should fail")
        }
        catch(e : SQLiteConstraintException){
            assert(e.localizedMessage.startsWith("FOREIGN KEY constraint failed", true))
        }
    }
}
